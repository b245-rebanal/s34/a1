const express = require("express");
const port = 3000;
const app = express();

let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }

];


//GET"/home"
app.get("/home", (request, response) => {
			response.status(200).send("Hello, Welcome to the homepage")
		})



//GET"/items"
app.get("/items", (request, response) => {
			response.status(200).send(items);
		})

//DELETE "/delete-item"
app.delete("/delete-item", (request, response)=>{

			let deleteditem = items.pop();

			response.send(deleteditem);
		})


app.listen(port, ()=> console.log("Server is running at port 3000"))